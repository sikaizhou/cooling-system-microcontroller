//*************************************************
//第一部分：申明引用函数文件
#include "mbed.h"
#include<string.h>
#include<math.h>

#define HTS221_I2C_READ    0xBF
#define HTS221_I2C_WRITE   0xBE

#define HTS221_WHO_AM_I_REG          0x0F//寄存器地址
#define HTS221_CTRL_REG1             0x20
#define HTS221_CTRL_REG2             0x21
#define HTS221_CTRL_REG3             0x22

#define HTS221_TEMP_H                0x2B
#define HTS221_TEMP_L                0x2A

#define HTS221_T0_OUT_H                0x3D
#define HTS221_T0_OUT_L                0x3C

#define HTS221_T1_OUT_H                0x3F
#define HTS221_T1_OUT_L                0x3E

#define HTS221_T0_degC_x8                0x32
#define HTS221_T1_degC_x8                0x33

#define HTS221_T1T0_msb                0x35
//第二部分：申明接口，该开发板的主要接口列表如下
/*
		LED1        = PA_5,       LED1
    LED2        = PA_5,       LED2 
    LED3        = PA_5,       LED3
    LED4        = PA_5,       LED4
    USER_BUTTON = PC_13,      用户按键
    SERIAL_TX   = PA_2,       串口发送
    SERIAL_RX   = PA_3,       串口接收
    USBTX       = PA_2,       USB发送
    USBRX       = PA_3,       USB接收
    I2C_SCL     = PB_8,       IIC的时钟线
    I2C_SDA     = PB_9,       IIC的数据线
    SPI_MOSI    = PA_7,       SPI的主设备输出/从设备输入
    SPI_MISO    = PA_6,       SPI的主设备输入/从设备输出
    SPI_SCK     = PA_5,       SPI的时钟
    SPI_CS      = PB_6,       SPI的片选信号
    PWM_OUT     = PB_3,       PWM输出
*/


I2C HTS221(I2C_SDA, I2C_SCL);

float mapFloat(int x, int in_min, int in_max, float out_min, float out_max)
{
   return ((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)/8;
}

struct PID{
	float SetPoint;//設定目標
	float Proportion;//比例常數	
	float Integral;//積分常數
	float Derivative;//微分常数
	float LastError;
	float PrevError;
	float SumError;
};

struct PID spid;
float rout;//Pid Response
float rin;//pid feedback
unsigned char flag,flag_1=0;
float high_time,low_time,count=0;//占空比参数
float temper;
float s;


//pid初始化
void PIDInit(struct PID *pp)
{
	memset(pp,0,sizeof(struct PID));
}

//计算pid
float PIDCalc(struct PID *pp, float error)
{
	float dError,Error;
	Error = error;//偏差
	pp->SumError += Error;//当前积分
	dError = pp->LastError - pp->PrevError;//当前微分
	pp->PrevError = pp->LastError;
	pp->LastError = Error;
	float result=(pp->Proportion * Error + pp->Integral * pp->SumError + pp->Derivative * dError);
	return result; //结果
}


//获得温度
float get_temper()
{
	char wrtie_REG1[2];
	char data_wrtie_REG2[2];
	char data_wrtie_REG3[2];
	char data_wrtie_REG4[2];
	
	char temper_H_date;
	char temper_L_date;
	char temper_H_add;
	char temper_L_add;
	
	char T0_H_date;
	char T0_L_date;
	char T0_H_add;
	char T0_L_add;
	int T0_date;
	
	char T1_H_date;
	char T1_L_date;
	char T1_H_add;
	char T1_L_add;
	int T1_date;
	
	char T0_x8_date;
	char T1_x8_date;
	char T0_x8_add;
	char T1_x8_add;
	int T0_x8_int_date;
	int T1_x8_int_date;
	
	char T1T0_msb_date;
	char T1T0_msb_add;
	
	int temper_date=0;
	float temperature;
	
	temper_H_add = HTS221_TEMP_H;
	temper_L_add = HTS221_TEMP_L;
	
	T0_H_add =  HTS221_T0_OUT_H;
	T0_L_add =  HTS221_T0_OUT_L;
	
	T1_H_add =  HTS221_T1_OUT_H;
	T1_L_add =  HTS221_T1_OUT_L;
	
	T0_x8_add = HTS221_T0_degC_x8;
	T1_x8_add = HTS221_T1_degC_x8;
	
	T1T0_msb_add = HTS221_T1T0_msb;
	
	
	
	wrtie_REG1[0]=HTS221_CTRL_REG1;
	wrtie_REG1[1]=0x83;     //设置传感器为active mode，BDU为0,即不停更新数据，速率为12.5Hz，1 0000 0 11
	
	HTS221.write( HTS221_I2C_WRITE , &T0_H_add, 1);
	HTS221.read( HTS221_I2C_READ , &T0_H_date, 1);     //读取高位
		
	HTS221.write( HTS221_I2C_WRITE , &T0_L_add, 1);
	HTS221.read( HTS221_I2C_READ , &T0_L_date, 1);     //读取低位
	
	HTS221.write( HTS221_I2C_WRITE , &T1_H_add, 1);
	HTS221.read( HTS221_I2C_READ , &T1_H_date, 1);     //读取高位
		
	HTS221.write( HTS221_I2C_WRITE , &T1_L_add, 1);
	HTS221.read( HTS221_I2C_READ , &T1_L_date, 1);     //读取低位
	
	HTS221.write( HTS221_I2C_WRITE , &T0_x8_add, 1);
	HTS221.read( HTS221_I2C_READ , &T0_x8_date, 1);     
		
	HTS221.write( HTS221_I2C_WRITE , &T1_x8_add, 1);
	HTS221.read( HTS221_I2C_READ , &T1_x8_date, 1);     
	
	HTS221.write( HTS221_I2C_WRITE , &T1T0_msb_add, 1);
	HTS221.read( HTS221_I2C_READ , &T1T0_msb_date, 1);   
	
	T0_date=(int)((int)T0_H_date<<8)|T0_L_date;  //合并 得到T0的adc输出数据
	T1_date=(int)((int)T1_H_date<<8)|T1_L_date;  //合并 得到T1的adc输出数据
	
	T0_x8_int_date=((T1T0_msb_date&0x03)<<8)|T0_x8_date;     //十位的，得到T0的度数乘以8的数据
	T1_x8_int_date=((T1T0_msb_date&0x0c)<<6)|T1_x8_date;     //十位的，得到T1的度数乘以8的数据
	
		HTS221.write( HTS221_I2C_WRITE , wrtie_REG1, 2,0);    //写入寄存器
		
		
		HTS221.write( HTS221_I2C_WRITE , &temper_H_add, 1);
		HTS221.read( HTS221_I2C_READ , &temper_H_date, 1);     //读取高位
		
		HTS221.write( HTS221_I2C_WRITE , &temper_L_add, 1);
		HTS221.read( HTS221_I2C_READ , &temper_L_date, 1);     //读取低位
		
		temper_date=(int)((int)temper_H_date<<8)|temper_L_date;  //合并
		temperature=mapFloat(temper_date,T0_date,T1_date,T0_x8_int_date,T1_x8_int_date);
		return temperature;
}


//温度比较处理
void compare_temper()
{
	if(spid.SetPoint<temper) //设置温度大于实际温度
	{
		if(temper-spid.SetPoint>1)
		{
			high_time=100;
			low_time=0;  //全速制冷
		}
		else
		{
			s=temper-spid.SetPoint;
			rin=s;
			rout=PIDCalc(&spid,rin);
			if(rout<=100)
				high_time=rout;
			else
			{
				high_time=100;
				low_time=0;
			}
		}
	}
	else
	{
		if(spid.SetPoint-temper>1)
		{
			high_time=100;
			low_time=0;  //全速制冷
		}
		else
		{
			s=spid.SetPoint-temper;
			rin=s;
			rout=PIDCalc(&spid,rin);
			if(rout<=100)
				high_time=rout;
			else
			{
				high_time=100;
				low_time=0;
	}
}	

PwmOut mypwm(PB_3);

//主函数V3,初始化设备，读取数据(单次读取)，并且进行计算，使用串口输出数据
int main(){
		PIDInit(&spid);
	  spid.Derivative=3; //设定各项参数
	  spid.Integral=0.3;
	  spid.Proportion=0.1;
	  spid.SetPoint=25;
	while(1)
	{
	  temper=get_temper();
	  compare_temper();
		printf("%.2f 'C\n",(double)temper); 
		mypwm.period_ms(100);
		mypwm.pulsewidth_ms(high_time);
	}
	return 0;

}